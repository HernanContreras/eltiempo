package test.inacap.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;
import org.json.JSONException;

public class MainActivity extends AppCompatActivity {

    private TextView Chillan, Santiago, Concepcion, Madrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.Chillan = (TextView)findViewById(R.id.Chillan);

//este es un comentario para git

        String url = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&appid=2163ce7f9e1fc3af51e709f3319dfd32&units=metric";
        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);


                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double temp = tempJSON.getDouble("temp");

                            Toast.makeText(getApplicationContext(), "La temperatura es" + temp , Toast.LENGTH_SHORT).show();
                            Chillan.setText("La temperatura en Chillán es: " + temp + "C°");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);
    }
}

